<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'local' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'rFYVgk3bhD5ZhaoxGrn6n6BwMLtyXhONzTr/QSbr8ntahrdpjClHMZ726oUAgmyF7i14RcfyreguWtraxDnoCw==');
define('SECURE_AUTH_KEY',  '7zBDOggthxVfULk9vkCkOi9bx2ses71iogy0BiMDZH819PiS2s2OmYB6C6uFq3N700UA17bHkKMC4XPCeb74Zw==');
define('LOGGED_IN_KEY',    'p1F0wQ2MM+u6YblT5lLUOAAAng7mWV3T81nrLxcZymIkjmiKh3KdGiGZ9+IkdEhuBmsFrX01H6Od4ExCsD1y4A==');
define('NONCE_KEY',        '/HCwn9n/rYiiKQzEtsGUcDwqT9RY3j/ZfJznvFKxgve2EbzTPwaSaP5lKNX752X3vXub8c34wgvM0go5pENw0Q==');
define('AUTH_SALT',        'v3PmVOUKkN+D8HTHVS6VfEPfkd9uj8iUURhTC+mQAj8C/SJ/i2HZgSJjx564Y6U96YVdLuhBCw1FNwUXwdDzNA==');
define('SECURE_AUTH_SALT', 'mFaDyOzOP+FC5bbxbcAV/sRz6+qkZ0rWPC+0fhG7PIMhNtSB6+gKf/23IpOsoR2hiOh7r2g+xz4o0XS9bD0YAQ==');
define('LOGGED_IN_SALT',   'CzyZwyUwK5WTj3IwNW1N8lOolwEPjT6/nuUUD1MU66DjoDV+xdAVl0lThMrE4gNeWmKxw8pnv+DUOAQvej+rZg==');
define('NONCE_SALT',       'EKMVOinkPZCjB1VI4hfFbrREw+50D+73G4mZ4iN7MzFR02zsJaUmFYNIZcFvHcPbOpmAuRVgNCijlfINogp6Tg==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
