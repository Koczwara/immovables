<?php

function immovables_files() {
  wp_enqueue_style('custom-google-fonts', '//fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,300;0,400;0,600;0,700;0,800;1,300;1,400;1,600;1,700;1,800&display=swap', '//fonts.googleapis.com/css2?family=Lato:ital,wght@0,100;0,300;0,400;0,700;0,900;1,100;1,300;1,400;1,700;1,900&display=swap','//fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap','' );
  wp_enqueue_style('font-awesome', '//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css');
  
  if (strstr($_SERVER['SERVER_NAME'], 'immovables.local')) {
    wp_enqueue_script('main-immovables-js', 'http://localhost:3000/bundled.js', NULL, '1.0', true);
  } else {
    wp_enqueue_script('our-vendors-js', get_theme_file_uri('/bundled-assets/vendors~scripts.5c98f5e899ceb6b2a75f.js'), NULL, '1.0', true);
    wp_enqueue_script('main-immovables-js', get_theme_file_uri('/bundled-assets/scripts.166e6a0854a259549d1e.js'), NULL, '1.0', true);
    wp_enqueue_style('our-main-styles', get_theme_file_uri('/bundled-assets/styles.166e6a0854a259549d1e.css'));
    wp_enqueue_style('our-vendors-styles', get_theme_file_uri('/bundled-assets/styles.5c98f5e899ceb6b2a75f.css'));
  }
  wp_localize_script('main-immovables-js', 'immovablesData', array(
    'root_url' => get_site_url()
  ));
  
}

add_action('wp_enqueue_scripts', 'immovables_files');

function immovables_features() {
  add_theme_support('title-tag');
}

add_action('after_setup_theme', 'immovables_features');

