<footer class="site-footer" id="footer">
      <div class="site-footer__inner container">
        <div class="group">

          <div class="site-footer__col-one">        
          <img src="<?php echo get_theme_file_uri('/images/wieniec.svg')?>" class="img-logo"><a href="<?php echo site_url() ?>" class="logo--footer">OSADA WYGODA<span></a>   
          </div>

          <div class="site-footer__col-two">
            <h6 class="site-footer--title">Biuro Sprzedaży</h6>
            <p class="site-footer__link">ul.Polna 42/6<br>00-635 Warszawa</p>
            <p><a class="site-footer__link" href="tel:22 102 47 88"><i class="fa fa-phone" aria-hidden="true"></i> Tel. 22 102 47 88</a></p>
          </div>
  
          <div class="site-footer__col-three">
            <h6 class="site-footer--title">Godziny Otwarcia</h6>
            <p>Pon-Pt: 9:00 - 17:00</p>
            <p>Sobota: nieczynne</p>
            <p>Niedziela: nieczynne</p>
          </div>

          <div class="site-footer__col-four">
            <h6 class="site-footer--title">Mapa Strony</h6>
              <nav class="footer-navigation">
                <ul>
                  <li><a href="#investment">Inwestycja</a></li>
                  <li><a href="#about-us">Inwestor</a></li>
                  <li><a href="#localization">Lokalizacja</a></li>
                  <li><a href="#flats">Mieszkania</a></li>
                  <li><a href="#gallery">Galeria</a></li>
                </ul>
              </nav>
          </div>

        </div>
      </div>


      <div class="site-footer__under__inner container">
        <div class="site-footer__under__inner__col-one">        
            <p class="privacy-policy">polityka prywatności</p>
        </div>
        <div class="site-footer__under__inner__col-two social-icons-list">      
          <a href="https://www.facebook.com/Osada-Wygoda-mieszkania-w-zielonym-sercu-Warszawy-100401905336322/?modal=admin_todo_tour&notif_id=1609187046176445&notif_t=page_invite&ref=notif" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a>
        </div>
        <div class="site-footer__under__inner__col-three">        
            <p>© 2020 Grupa Hetman | by <a href="http://patrycjakoczwara.pl/" target="_blank">Patrycja Koczwara</a></p>
        </div>
      </div>

    </footer>
<?php wp_footer(); ?>
</body>
</html>