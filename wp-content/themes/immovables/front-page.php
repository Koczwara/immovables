<?php get_header(); ?>

 <?php get_template_part('partials/main'); ?>
 <?php get_template_part('partials/investment'); ?>
 <?php get_template_part('partials/about-us'); ?>
 <?php get_template_part('partials/localization'); ?>
 <?php get_template_part('partials/flats'); ?>
 <?php get_template_part('partials/gallery'); ?>
 <?php get_template_part('partials/contact'); ?>

<?php get_footer(); ?>
