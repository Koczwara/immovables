<!DOCTYPE html>
<html <?php language_attributes(); ?>>
  <head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php wp_head(); ?>
  </head>
  <body <?php body_class(); ?>>

  
  <header class="site-header">
    <div class="container">
       <img src="<?php echo get_theme_file_uri('/images/wieniec.svg')?>" class="img-logo"><a href="<?php echo site_url() ?>" class="logo">OSADA WYGODA<span></a>   
      <i class="site-header__menu-trigger fa fa-bars" aria-hidden="true"></i>
      <div class="site-header__menu group">
        <nav class="nav main-navigation">
          <ul>
            <li><a href="#investment">INWESTYCJA</a></li>
            <li><a href="#about-us">INWESTOR</a></li>
            <li><a href="#localization">LOKALIZACJA</a></li>
            <li><a href="#flats">MIESZKANIA</a></li>
            <li><a href="#gallery">GALERIA</a></li>
            <li><a href="#contact">KONTAKT</a></li>
          </ul>
        </nav>
      </div>
    </div>
  </header>