<section class="investment" id="investment">
   <div class="container investment__content">
      <div class="investment__col-two">
            <span class="investment__case-number" data-aos="zoom-in" data-aos-duration="2000">38</span>
            <span class="investment__case-description">
               APARTAMENTÓW
            </span>
            <div class="btn-place only-desktop">
               <a href="#contact" class="btn btn--black">
                  Dowiedz się więcej
               </a>
            </div>
      </div>
      <div class="investment__col-one">
         <h1 class="investment__content--title" data-aos="fade-up" data-aos-duration="3000">
            Osada Wygoda
         </h1>
         <h2 class="investment__content--subtitle" data-aos="fade-up" data-aos-duration="3000">
            Kameralna inwestycja w zielonym sercu Warszawy
         </h2>
         <div class="investment__content--description" data-aos="fade-up" data-aos-duration="3000">
         <p>
            Osada Wygoda to kameralny inwestycja składająca się 38 apartamentów. Projekt idealnie wkomponowuje się w okolicę ze swoją 3-piętrową bryłą budynku.  Dostępne są lokale o różnym metrażu od 1 do 3 pokojowych. Zaprojektowaliśmy także kilka wyjątkowych, 4 i 5 pokojowych apartamentów rodzinnych. Inwestycję wyróżniają widne kuchnie i idealnie rozplanowana przestrzeń. Dbając o wygodę mieszkańców gwarantujemy duży wybór komórek lokatorskich i miejsc postojowych w garażu podziemnym. 
            Do dyspozycji mieszkańców są także rowerownie, po jednej przy wejściu do każdej z klatek schodowych i jedna na poziomie garażu. 
         </p>
         <h4 class="investment__content--description--title"> OSADA WYGODA TO TEŻ  MIEJSCE O CIEKAWEJ HISTORII.</h4>
         <p>
            Nazwa inwestycji – nawiązuje do bogatej historii tej części Wawra. Po przeniesieniu stolicy do Warszawy zanotowano tutaj ożywienie gospodarcze. Miały tu miejsce niecodzienne wydarzenia m.in. zjazd szlachecki zakończony wyborem Henryka Walezego na króla Polski. Tereny te wielokrotnie wchodziły w skład majątków znanych rodzin szlacheckich - jednym z nich była właśnie OSADA WYGODA. Rozwój kolei i sieci dróg spowodował napływ ludności i rozwój gospodarczy tego terenu. Osada Wygoda była atrakcyjna pod względem lokalizacji i walorów środowiskowych: lasy, plaże nadwiślańskie. Powstawały tutaj liczne letniska. Tak jest do dzisiaj - to miejsce, gdzie niewątpliwie ,,dobrze się żyje”.
         </p>

         </div>
         <div class="btn-place btn-mobile only-mobile">
               <a href="#contact" class="btn btn--black">
                  Dowiedz się więcej
               </a>
         </div>
      </div>
   </div>
</section>