<section class="page-banner" style="background-image: url(<?php echo get_theme_file_uri('/images/main1.jpg')?>);">
   <div class="page-banner__overlay">
      <div class="">
         <h1 class="page-banner--title heading heading--main" data-aos="zoom-in" data-aos-duration="3000">Osada Wygoda</h1>
         <h2 class="page-banner--subtitle heading--medium" data-aos="fade-up" data-aos-anchor-placement="top-bottom" data-aos-delay="1000"  data-aos-duration="2000">
            APARTAMENTY W ZIELONYM SERCU WARSZAWY
         </h2>
         <div class="page-banner--button-place">
            <a href="#investment" class="btn btn--white">
               Dowiedz się więcej
            </a>
         </div>
      </div>
   </div>
</section>