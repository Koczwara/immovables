<section class="localization" id="localization">
  <div class="container localization__content">
    <div class="localization__col-two">
         <span class="localization__case-number" data-aos="zoom-in" data-aos-duration="2000">30</span>
         <span class="localization__case-description">MIN OD CENTRUM</span>
         <div class="btn-place only-desktop">
            <a href="#glide1" class="btn btn--black">
               Dowiedz się więcej
            </a>
         </div>
    </div>
    <div class="localization__col-one">
       <h1 class="localization__content--title" data-aos="fade-up" data-aos-duration="3000">
         Lokalizacja
       </h1>
       <h2 class="localization__content--subtitle" data-aos="fade-up" data-aos-duration="3000">
          Lokalizacja to ogromna zaleta inwestycji Osada Wygoda.
       </h2>
       <p class="localization__content--description" data-aos="fade-up" data-aos-duration="3000">
         Położona jest przy ulicy Agrestowej 4 w
         dzielnicy Wawer, tuż obok stacji kolejowej Warszawa Miedzeszyn. Niedaleko powstaje Most
         Południowy i trasa S2 co już niedługo zagwarantuje szybkie połączenie z zachodnią częścią Warszawy
         a także idealny wyjazd z miasta.
         Niewątpliwym atutem jest bliskość terenów zielonych. Miedzeszyn to część Wawra położona wśród
         lasów z bogactwem wielu gatunków drzew i roślin. Pełno tutaj ścieżek rowerowych i pieszych. To
         idealne miejsce na odpoczynek po pracy.
       </p>
       <div class="btn-place btn-mobile only-mobile">
            <a href="#glide1" class="btn btn--black">
               Dowiedz się więcej
            </a>
       </div>
      </div>
    </div>

    <div class="glide hero-slider container" id="glide1">
    <div class="glide__arrows" data-glide-el="controls">
				<button class="glide__arrow glide__arrow--left" data-glide-dir="<" data-disabled="true"><</button>
				<button class="glide__arrow glide__arrow--right" data-glide-dir=">" data-disabled="false">></button>
			</div>

       <div class="glide__track" data-glide-el="track">
         <ul class="glide__slides">
         <li class="hero-slider__slide">
            <div class="hero-slider__box hero-slider__box--pink">
               <span class="hero-slider__box--min">4</span><span class="hero-slider__box--description">min</span><span><img class="hero-slider--img" src="<?php echo get_theme_file_uri('/images/walk.svg')?>"></span>
               <p class="hero-slider__description">Stacja SKM Warszawa Miedzeszyn</p>
            </div>
         </li>
         <li class="hero-slider__slide">
               <div class="hero-slider__box hero-slider__box--green">
                  <span class="hero-slider__box--min">5</span><span class="hero-slider__box--description">min</span><span><img class="hero-slider--img" src="<?php echo get_theme_file_uri('/images/car.svg')?>"></span>
                  <p class="hero-slider__description">Plaża i tereny zielone nad Wisłą</p>
               </div>
         </li>
         <li class="hero-slider__slide">
            <div class="hero-slider__box hero-slider__box--blue">
               <span class="hero-slider__box--min">29</span><span class="hero-slider__box--description">min</span><span><img class="hero-slider--img" src="<?php echo get_theme_file_uri('/images/bus.svg')?>"></span>
               <p class="hero-slider__description">Centrum ( SKM - odjazdy co 15min)</p>
            </div>
        </li>
        <li class="hero-slider__slide">
            <div class="hero-slider__box hero-slider__box--pink">
               <span class="hero-slider__box--min">5</span><span class="hero-slider__box--description">min</span><span><img class="hero-slider--img" src="<?php echo get_theme_file_uri('/images/walk.svg')?>"></span>
               <p class="hero-slider__description">Biedronka</p>
            </div>
         </li>
         <li class="hero-slider__slide">
               <div class="hero-slider__box hero-slider__box--green">
                  <span class="hero-slider__box--min">8</span><span class="hero-slider__box--description">min</span><span><img class="hero-slider--img" src="<?php echo get_theme_file_uri('/images/car.svg')?>"></span>
                  <p class="hero-slider__description">Jezioro Torfy</p>
               </div>
         </li>
         <li class="hero-slider__slide">
            <div class="hero-slider__box hero-slider__box--blue">
               <span class="hero-slider__box--min">20</span><span class="hero-slider__box--description">min</span><span><img class="hero-slider--img" src="<?php echo get_theme_file_uri('/images/bus.svg')?>"></span>
               <p class="hero-slider__description">Stacja PKP Warszawa Wschodnia</p>
            </div>
        </li>
        <li class="hero-slider__slide">
            <div class="hero-slider__box hero-slider__box--pink">
               <span class="hero-slider__box--min">8</span><span class="hero-slider__box--description">min</span><span><img class="hero-slider--img" src="<?php echo get_theme_file_uri('/images/walk.svg')?>"></span>
               <p class="hero-slider__description">Stajnia Jackowo</p>
            </div>
         </li>
         <li class="hero-slider__slide">
               <div class="hero-slider__box hero-slider__box--green">
                  <span class="hero-slider__box--min">5</span><span class="hero-slider__box--description">min</span><span><img class="hero-slider--img" src="<?php echo get_theme_file_uri('/images/car.svg')?>"></span>
                  <p class="hero-slider__description">Most Południowy</p>
               </div>
         </li>
         <li class="hero-slider__slide">
            <div class="hero-slider__box hero-slider__box--blue">
               <span class="hero-slider__box--min">3</span><span class="hero-slider__box--description">min</span><span><img class="hero-slider--img" src="<?php echo get_theme_file_uri('/images/walk.svg')?>"></span>
               <p class="hero-slider__description">Plac Zabaw</p>
            </div>
        </li>
        <li class="hero-slider__slide">
            <div class="hero-slider__box hero-slider__box--pink">
               <span class="hero-slider__box--min">3</span><span class="hero-slider__box--description">min</span><span><img class="hero-slider--img" src="<?php echo get_theme_file_uri('/images/car.svg')?>"></span>
               <p class="hero-slider__description">Południowa Obwodnica Warszawy</p>
            </div>
         </li>
         <li class="hero-slider__slide">
               <div class="hero-slider__box hero-slider__box--green">
                  <span class="hero-slider__box--min">3</span><span class="hero-slider__box--description">min</span><span><img class="hero-slider--img" src="<?php echo get_theme_file_uri('/images/walk.svg')?>"></span>
                  <p class="hero-slider__description">leśne ścieżki piesze i rowerowe</p>
               </div>
         </li>
         <li class="hero-slider__slide">
            <div class="hero-slider__box hero-slider__box--blue">
               <span class="hero-slider__box--min">10</span><span class="hero-slider__box--description">min</span><span><img class="hero-slider--img" src="<?php echo get_theme_file_uri('/images/car.svg')?>"></span>
               <p class="hero-slider__description">basen</p>
            </div>
        </li>
        <li class="hero-slider__slide">
            <div class="hero-slider__box hero-slider__box--pink">
               <span class="hero-slider__box--min">14</span><span class="hero-slider__box--description">min</span><span><img class="hero-slider--img" src="<?php echo get_theme_file_uri('/images/walk.svg')?>"></span>
               <p class="hero-slider__description">XXV Liceum Ogólnokształcące</p>
            </div>
         </li>
         <li class="hero-slider__slide hero-slider__box--green">
               <div class="hero-slider__box hero-slider__box--green">
                  <span class="hero-slider__box--min">5</span><span class="hero-slider__box--description">min</span><span><img class="hero-slider--img" src="<?php echo get_theme_file_uri('/images/car.svg')?>"></span>
                  <p class="hero-slider__description">zabytkowa kino - kawiarnia w Falenicy</p>
               </div>
         </li>
         <li class="hero-slider__slide">
            <div class="hero-slider__box hero-slider__box--blue">
               <span class="hero-slider__box--min">10</span><span class="hero-slider__box--description">min</span><span><img class="hero-slider--img" src="<?php echo get_theme_file_uri('/images/walk.svg')?>"></span>
               <p class="hero-slider__description">Szkoła Podstawowa nr 216</p>
            </div>
        </li>
      </ul>
    </div> 
  </div>
</section>