<section class="gallery-section container" id="gallery">
    <h1 class="gallery-section--title heading" data-aos="zoom-in" data-aos-duration="3000">Galeria</h1>
    <div class="container button-container">
    <div class="btn-place btn-one">
        <a class="btn btn--black">
            Wizualizacje
        </a>
    </div>
    <div class="btn-place btn-two">
        <a class="btn btn--black">
            Zdjęcia z budowy
        </a>
    </div>
    </div>
    <div class="gallery-one">
        <div class="container">
            <div class="synch-carousels">
                <div class="left child">
                    <div class="gallery">
                    <div class="item">
                        <img src="<?php echo get_theme_file_uri('/images/wiz4.jpg')?>">
                    </div>
                    <div class="item">
                        <img src="<?php echo get_theme_file_uri('/images/wiz1.jpg')?>">
                    </div>
                    <div class="item">
                        <img src="<?php echo get_theme_file_uri('/images/wiz3.jpg')?>">
                    </div>
                    <div class="item">
                        <img src="<?php echo get_theme_file_uri('/images/wiz2.jpg')?>">
                    </div>
                    <div class="item">
                        <img src="<?php echo get_theme_file_uri('/images/wiz7.jpg')?>">
                    </div>
                    </div>
                </div>

            <div class="right child">
                <div class="gallery2">
                    <div class="item">
                        <img src="<?php echo get_theme_file_uri('/images/wiz4.jpg')?>">
                    </div>
                    <div class="item">
                        <img src="<?php echo get_theme_file_uri('/images/wiz1.jpg')?>">
                    </div>
                    <div class="item">
                        <img src="<?php echo get_theme_file_uri('/images/wiz3.jpg')?>">
                    </div>
                    <div class="item">
                        <img src="<?php echo get_theme_file_uri('/images/wiz2.jpg')?>">
                    </div>
                    <div class="item">
                        <img src="<?php echo get_theme_file_uri('/images/wiz7.jpg')?>">
                    </div>
                    </div>
                    <div class="nav-arrows">
                    <button class="arrow-left">
                        <svg width="24" height="24" xmlns="http://www.w3.org/2000/svg" fill-rule="evenodd" clip-rule="evenodd"><path d="M2.117 12l7.527 6.235-.644.765-9-7.521 9-7.479.645.764-7.529 6.236h21.884v1h-21.883z"/></svg>
                    </button>
                    <button class="arrow-right">
                        <svg width="24" height="24" xmlns="http://www.w3.org/2000/svg" fill-rule="evenodd" clip-rule="evenodd"><path d="M21.883 12l-7.527 6.235.644.765 9-7.521-9-7.479-.645.764 7.529 6.236h-21.884v1h21.883z"/></svg>
                        </button>
                    </div>
                    <div class="photos-counter">
                    <span></span><span></span>
                    </div>
                </div>
            </div>
        </div>
    </div>

<div class="gallery-two">
    <div class="container">
        <div class="synch-carousels">
            <div class="left-two child">
                <div class="gallery_one">
                <div class="item">
                    <img src="<?php echo get_theme_file_uri('/images/bud1.jpeg')?>">
                </div>
                <div class="item">
                    <img src="<?php echo get_theme_file_uri('/images/bud2.jpeg')?>">
                </div>
                <div class="item">
                    <img src="<?php echo get_theme_file_uri('/images/bud3.jpeg')?>">
                </div>
                <div class="item">
                    <img src="<?php echo get_theme_file_uri('/images/newbud.jpg')?>">
                </div>
                <div class="item">
                    <img src="<?php echo get_theme_file_uri('/images/image0.jpeg')?>">
                </div>
                <div class="item">
                    <img src="<?php echo get_theme_file_uri('/images/image1.jpeg')?>">
                </div>
                <div class="item">
                    <img src="<?php echo get_theme_file_uri('/images/image2.jpeg')?>">
                </div>
                <div class="item">
                    <img src="<?php echo get_theme_file_uri('/images/lastone.jpg')?>">
                </div>
                </div>
            </div>

            <div class="right child">
                <div class="gallery_two">
                <div class="item">
                        <img src="<?php echo get_theme_file_uri('/images/bud1.jpeg')?>">
                </div>
                <div class="item">
                    <img src="<?php echo get_theme_file_uri('/images/bud2.jpeg')?>">
                </div>
                <div class="item">
                    <img src="<?php echo get_theme_file_uri('/images/bud3.jpeg')?>">
                </div>
                <div class="item">
                    <img src="<?php echo get_theme_file_uri('/images/newbud.jpg')?>">
                </div>
                <div class="item">
                    <img src="<?php echo get_theme_file_uri('/images/image0.jpeg')?>">
                </div>
                <div class="item">
                    <img src="<?php echo get_theme_file_uri('/images/image1.jpeg')?>">
                </div>
                <div class="item">
                    <img src="<?php echo get_theme_file_uri('/images/image2.jpeg')?>">
                </div>
                <div class="item">
                    <img src="<?php echo get_theme_file_uri('/images/lastone.jpg')?>">
                </div>
  
                </div>
                <div class="nav-arrows">
                <button class="arrow-left-two">
                    <svg width="24" height="24" xmlns="http://www.w3.org/2000/svg" fill-rule="evenodd" clip-rule="evenodd"><path d="M2.117 12l7.527 6.235-.644.765-9-7.521 9-7.479.645.764-7.529 6.236h21.884v1h-21.883z"/></svg>
                </button>
                <button class="arrow-right-two">
                    <svg width="24" height="24" xmlns="http://www.w3.org/2000/svg" fill-rule="evenodd" clip-rule="evenodd"><path d="M21.883 12l-7.527 6.235.644.765 9-7.521-9-7.479-.645.764 7.529 6.236h-21.884v1h21.883z"/></svg>
                    </button>
                </div>
                <div class="photos-counter-two">
                <span></span><span></span>
                </div>
            </div>
        </div>
    </div>
</div>
</section>