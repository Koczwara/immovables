<section class="contact" id="contact">
    <div class="container">
       <div class="contact--content contact--content--col-one">
         <h1 class="contact--title heading">
            Biuro sprzedaży
         </h1>
         <h2 class="contact--subtitle">
            Spotkania po wcześniejszym umówieniu się.
         </h2>
      </div>
      <div class="team-container">
         <div class="contact__col-one">
               <div class="team-member">
                  <div class="team-img">
                     <img src="<?php echo get_theme_file_uri('/images/dorota.jpg')?>" alt="team member" class="img-responsive">
                  </div>
               </div>
               <div class="team-title">
                  <h4 class="team-title--name">Dorota Połujańska</h4>
                  <h5 class="team-title--position">Opiekun Klienta</h5>
                  <p><a class="desk__link" href="tel:508378222">tel: 508 378 222</a></p>
               </div>
         </div>
         <div class="contact__col-two">
               <div class="team-member">
                  <div class="team-img">
                     <img src="<?php echo get_theme_file_uri('/images/damian.jpg')?>" alt="team member" class="img-responsive">
                  </div>
               </div>
               <div class="team-title">
                  <h4 class="team-title--name">Damian Potębski</h4>
                  <h5 class="team-title--position">Opiekun Klienta</h5>
                  <p><a class="desk__link" href="tel:508 378 219">tel: 508 378 219</a></p>
               </div>
         </div>
      </div>

      <div class="contact--content contact--content--col-two">
         <h1 class="contact--title--two heading">
            Dział Kredytów Hipotecznych
         </h1>
         <h2 class="contact--subtitle only-desktop">
            Finansowanie zakupu naszych mieszkań realizujemy przy współpracy z<a href="http://www.conse.pl/" target="_blank"><img src="<?php echo get_theme_file_uri('/images/logo.png')?>" alt="logo" class="logo-conse"></a>. Eksperci mający ponad 10 letnie doświadczenie w branży przedstawią ofertę i pomogą zorganizować finansowanie we wszystkich dostępnych na rynku bankach, które oferują kredyty hipoteczne.
         </h2>
         <h2 class="contact--subtitle only-mobile">
            Finansowanie zakupu naszych mieszkań realizujemy przy współpracy z<br><a href="http://www.conse.pl/" target="_blank"><img src="<?php echo get_theme_file_uri('/images/logo.png')?>" alt="logo" class="logo-conse"></a>.<br> Eksperci mający ponad 10 letnie doświadczenie w branży przedstawią ofertę i pomogą zorganizować finansowanie we wszystkich dostępnych na rynku bankach, które oferują kredyty hipoteczne.
         </h2>
      </div>
      <div class="team-container">
         <div class="contact__col-one contact--col-one">
                  <div class="team-title">
                     <h1 class="team-title--name">Łukasz Szynkaruk</h1>
                     <a class="team-title--mail--link" href="mailto:lukasz.szynkaruk@conse.pl"><h5 class="team-title--mail">lukasz.szynkaruk@conse.pl</h5></a>
                     <p><a class="desk__link" href="tel:791 777 88">tel: 791 777 88</a></p>
                  </div>
            </div>
            <div class="contact__col-two contact--col-two">
                  <div class="team-title">
                     <h1 class="team-title--name">Andrzej Brudzyński</h1>
                     <a class="team-title--mail--link" href="mailto:andrzej.brudzynski@conse.pl"><h5 class="team-title--mail">andrzej.brudzynski@conse.pl</h5></a>
                     <p><a class="desk__link" href="tel:696 083 837">tel: 696 083 837</a></p>
                  </div>
            </div>
         </div>
    </div>
</section>