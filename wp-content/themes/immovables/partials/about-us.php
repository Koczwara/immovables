<section class="about-us" id="about-us">
    <div class="container about-us__content">
      <div class="about-us__col-one">
       <h1 class="about-us__content--title" data-aos="fade-up" data-aos-duration="3000">
         Grupa Inwestycyjna Hetman Sp. z o.o.
       </h1>
       <h2 class="about-us__content--subtitle" data-aos="fade-up" data-aos-duration="3000">
         Stabilna firma o ugruntowanej pozycji na rynku.
       </h2>
       <p class="about-us__content--description" data-aos="fade-up" data-aos-duration="3000">
         Grupa Inwestycyjna Hetman Sp. z o.o. może pochwalić się już 26 latami doświadczenia. Jest
         firmą deweloperską, która powstała w marcu 1995 roku na bazie działającej od 1989r spółki
         budowlanej PHU Renus sp. c.
         Od 2003 Grupa Inwestycyjna Hetman sp. z o.o. jest członkiem Polskiego Związku Firm
         Deweloperskich.
         Założycielami i właścicielami, a także Członkami Zarządu spółki są osoby posiadające
         wykształcenie techniczne o kierunku budowlanym i długoletnie doświadczenie na
         stanowiskach kierowniczych w bezpośrednim wykonawstwie na warszawskich budowach.
       </p>
         <div class="btn-place btn-mobile only-mobile">
            <a href="http://hetman.com.pl/realizacje" target="_blank" class="btn btn--black">
               Zrealizowane inwestycje
            </a>
         </div>
      </div>
      <div class="about-us__col-two only-desktop">
         <span class="about-us__case-number" data-aos="zoom-in" data-aos-duration="2000">26</span>
         <span class="about-us__case-description">LATA DOŚWIADCZENIA</span>
         <div class="btn-place only-desktop">
            <a href="http://hetman.com.pl/realizacje" target="_blank" class="btn btn--black">
               Zrealizowane inwestycje
            </a>
         </div>
      </div>
    </div>
</section>
