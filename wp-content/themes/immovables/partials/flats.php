
<header class="img-box" style="background-image: url(<?php echo get_theme_file_uri('/images/wiz4.jpg')?>);">
  <h1 class="title heading">Dostępne lokale</h1>
</header>

<section class="flats" id="flats">
    <div class="container flats-tabel">
        <div class="flats-tabel__col-one">        
            <p class="flats-tabel--title">Nr lokalu</p>
            <p class="flats-tabel--number"><?php the_field('number_a01') ?></p>
            <p class="flats-tabel--number"><?php the_field('number_a02') ?></p>
            <p class="flats-tabel--number"><?php the_field('number_b01') ?></p>
            <p class="flats-tabel--number"><?php the_field('number_b02') ?></p>
            <p class="flats-tabel--number"><?php the_field('number_b03') ?></p>
            <p class="flats-tabel--number"><?php the_field('number_a11') ?></p>
            <p class="flats-tabel--number"><?php the_field('number_a12') ?></p>
            <p class="flats-tabel--number"><?php the_field('number_a13') ?></p>
            <p class="flats-tabel--number"><?php the_field('number_a14') ?></p>
            <p class="flats-tabel--number"><?php the_field('number_a15') ?></p>
            <p class="flats-tabel--number"><?php the_field('number_b11') ?></p>
            <p class="flats-tabel--number"><?php the_field('number_b12') ?></p>
            <p class="flats-tabel--number"><?php the_field('number_b13') ?></p>
            <p class="flats-tabel--number"><?php the_field('number_b14') ?></p>
            <p class="flats-tabel--number"><?php the_field('number_b15') ?></p>
            <p class="flats-tabel--number"><?php the_field('number_b16') ?></p>
            <p class="flats-tabel--number"><?php the_field('number_a21') ?></p>
            <p class="flats-tabel--number"><?php the_field('number_a22') ?></p>
            <p class="flats-tabel--number"><?php the_field('number_a23') ?></p>
            <p class="flats-tabel--number"><?php the_field('number_a24') ?></p>
            <p class="flats-tabel--number"><?php the_field('number_a25') ?></p>
            <p class="flats-tabel--number"><?php the_field('number_b21') ?></p>
            <p class="flats-tabel--number"><?php the_field('number_b22') ?></p>
            <p class="flats-tabel--number"><?php the_field('number_b23') ?></p>
            <p class="flats-tabel--number"><?php the_field('number_b24') ?></p>
            <p class="flats-tabel--number"><?php the_field('number_b25') ?></p>
            <p class="flats-tabel--number"><?php the_field('number_b26') ?></p>
            <p class="flats-tabel--number"><?php the_field('number_a31') ?></p>
            <p class="flats-tabel--number"><?php the_field('number_a32') ?></p>
            <p class="flats-tabel--number"><?php the_field('number_a33') ?></p>
            <p class="flats-tabel--number"><?php the_field('number_a34') ?></p>
            <p class="flats-tabel--number"><?php the_field('number_a35') ?></p>
            <p class="flats-tabel--number"><?php the_field('number_b31') ?></p>
            <p class="flats-tabel--number"><?php the_field('number_b32') ?></p>
            <p class="flats-tabel--number"><?php the_field('number_b33') ?></p>
            <p class="flats-tabel--number"><?php the_field('number_b34') ?></p>
            <p class="flats-tabel--number"><?php the_field('number_b35') ?></p>
            <p class="flats-tabel--number"><?php the_field('number_b36') ?></p>
        </div>
        <div class="flats-tabel__col-two">        
            <p class="flats-tabel--title">Piętro</p>
            <p class="flats-tabel--floor"><?php the_field('floor_a01') ?></p>
            <p class="flats-tabel--floor"><?php the_field('floor_a02') ?></p>
            <p class="flats-tabel--floor"><?php the_field('floor_b01') ?></p>
            <p class="flats-tabel--floor"><?php the_field('floor_b02') ?></p>
            <p class="flats-tabel--floor"><?php the_field('floor_b03') ?></p>
            <p class="flats-tabel--floor"><?php the_field('floor_a11') ?></p>
            <p class="flats-tabel--floor"><?php the_field('floor_a12') ?></p>
            <p class="flats-tabel--floor"><?php the_field('floor_a13') ?></p>
            <p class="flats-tabel--floor"><?php the_field('floor_a14') ?></p>
            <p class="flats-tabel--floor"><?php the_field('floor_a15') ?></p>
            <p class="flats-tabel--floor"><?php the_field('floor_b11') ?></p>
            <p class="flats-tabel--floor"><?php the_field('floor_b12') ?></p>
            <p class="flats-tabel--floor"><?php the_field('floor_b13') ?></p>
            <p class="flats-tabel--floor"><?php the_field('floor_b14') ?></p>
            <p class="flats-tabel--floor"><?php the_field('floor_b15') ?></p>
            <p class="flats-tabel--floor"><?php the_field('floor_b16') ?></p>
            <p class="flats-tabel--floor"><?php the_field('floor_a21') ?></p>
            <p class="flats-tabel--floor"><?php the_field('floor_a22') ?></p>
            <p class="flats-tabel--floor"><?php the_field('floor_a23') ?></p>
            <p class="flats-tabel--floor"><?php the_field('floor_a24') ?></p>
            <p class="flats-tabel--floor"><?php the_field('floor_a25') ?></p>
            <p class="flats-tabel--floor"><?php the_field('floor_b21') ?></p>
            <p class="flats-tabel--floor"><?php the_field('floor_b22') ?></p>
            <p class="flats-tabel--floor"><?php the_field('floor_b23') ?></p>
            <p class="flats-tabel--floor"><?php the_field('floor_b24') ?></p>
            <p class="flats-tabel--floor"><?php the_field('floor_b25') ?></p>
            <p class="flats-tabel--floor"><?php the_field('floor_b26') ?></p>
            <p class="flats-tabel--floor"><?php the_field('floor_a31') ?></p>
            <p class="flats-tabel--floor"><?php the_field('floor_a32') ?></p>
            <p class="flats-tabel--floor"><?php the_field('floor_a33') ?></p>
            <p class="flats-tabel--floor"><?php the_field('floor_a34') ?></p>
            <p class="flats-tabel--floor"><?php the_field('floor_a35') ?></p>
            <p class="flats-tabel--floor"><?php the_field('floor_b31') ?></p>
            <p class="flats-tabel--floor"><?php the_field('floor_b32') ?></p>
            <p class="flats-tabel--floor"><?php the_field('floor_b33') ?></p>
            <p class="flats-tabel--floor"><?php the_field('floor_b34') ?></p>
            <p class="flats-tabel--floor"><?php the_field('floor_b35') ?></p>
            <p class="flats-tabel--floor"><?php the_field('floor_b36') ?></p>
        </div>
        <div class="flats-tabel__col-three">        
            <p class="flats-tabel--title">Liczba pokoi</p>
            <p class="flats-tabel--room"><?php the_field('room_number_a01') ?></p>
            <p class="flats-tabel--room"><?php the_field('room_number_a02') ?></p>
            <p class="flats-tabel--room"><?php the_field('room_number_b01') ?></p>
            <p class="flats-tabel--room"><?php the_field('room_number_b02') ?></p>
            <p class="flats-tabel--room"><?php the_field('room_number_b03') ?></p>
            <p class="flats-tabel--room"><?php the_field('room_number_a11') ?></p>
            <p class="flats-tabel--room"><?php the_field('room_number_a12') ?></p>
            <p class="flats-tabel--room"><?php the_field('room_number_a13') ?></p>
            <p class="flats-tabel--room"><?php the_field('room_number_a14') ?></p>
            <p class="flats-tabel--room"><?php the_field('room_number_a15') ?></p>
            <p class="flats-tabel--room"><?php the_field('room_number_b11') ?></p>
            <p class="flats-tabel--room"><?php the_field('room_number_b12') ?></p>
            <p class="flats-tabel--room"><?php the_field('room_number_b13') ?></p>
            <p class="flats-tabel--room"><?php the_field('room_number_b14') ?></p>
            <p class="flats-tabel--room"><?php the_field('room_number_b15') ?></p>
            <p class="flats-tabel--room"><?php the_field('room_number_b16') ?></p>
            <p class="flats-tabel--room"><?php the_field('room_number_a21') ?></p>
            <p class="flats-tabel--room"><?php the_field('room_number_a22') ?></p>
            <p class="flats-tabel--room"><?php the_field('room_number_a23') ?></p>
            <p class="flats-tabel--room"><?php the_field('room_number_a24') ?></p>
            <p class="flats-tabel--room"><?php the_field('room_number_a25') ?></p>
            <p class="flats-tabel--room"><?php the_field('room_number_b21') ?></p>
            <p class="flats-tabel--room"><?php the_field('room_number_b22') ?></p>
            <p class="flats-tabel--room"><?php the_field('room_number_b23') ?></p>
            <p class="flats-tabel--room"><?php the_field('room_number_b24') ?></p>
            <p class="flats-tabel--room"><?php the_field('room_number_b25') ?></p>
            <p class="flats-tabel--room"><?php the_field('room_number_b26') ?></p>
            <p class="flats-tabel--room"><?php the_field('room_number_a31') ?></p>
            <p class="flats-tabel--room"><?php the_field('room_number_a32') ?></p>
            <p class="flats-tabel--room"><?php the_field('room_number_a33') ?></p>
            <p class="flats-tabel--room"><?php the_field('room_number_a34') ?></p>
            <p class="flats-tabel--room"><?php the_field('room_number_a35') ?></p>
            <p class="flats-tabel--room"><?php the_field('room_number_b31') ?></p>
            <p class="flats-tabel--room"><?php the_field('room_number_b32') ?></p>
            <p class="flats-tabel--room"><?php the_field('room_number_b33') ?></p>
            <p class="flats-tabel--room"><?php the_field('room_number_b34') ?></p>
            <p class="flats-tabel--room"><?php the_field('room_number_b35') ?></p>
            <p class="flats-tabel--room"><?php the_field('room_number_b36') ?></p>
        </div>
        <div class="flats-tabel__col-four">        
            <p class="flats-tabel--title">Metraż</p>
            <p class="flats-tabel--yardage"><?php the_field('yardage_a01') ?></p>
            <p class="flats-tabel--yardage"><?php the_field('yardage_a02') ?></p>
            <p class="flats-tabel--yardage"><?php the_field('yardage_b01') ?></p>
            <p class="flats-tabel--yardage"><?php the_field('yardage_b02') ?></p>
            <p class="flats-tabel--yardage"><?php the_field('yardage_b03') ?></p>
            <p class="flats-tabel--yardage"><?php the_field('yardage_a11') ?></p>
            <p class="flats-tabel--yardage"><?php the_field('yardage_a12') ?></p>
            <p class="flats-tabel--yardage"><?php the_field('yardage_a13') ?></p>
            <p class="flats-tabel--yardage"><?php the_field('yardage_a14') ?></p>
            <p class="flats-tabel--yardage"><?php the_field('yardage_a15') ?></p>
            <p class="flats-tabel--yardage"><?php the_field('yardage_b11') ?></p>
            <p class="flats-tabel--yardage"><?php the_field('yardage_b12') ?></p>
            <p class="flats-tabel--yardage"><?php the_field('yardage_b13') ?></p>
            <p class="flats-tabel--yardage"><?php the_field('yardage_b14') ?></p>
            <p class="flats-tabel--yardage"><?php the_field('yardage_b15') ?></p>
            <p class="flats-tabel--yardage"><?php the_field('yardage_b16') ?></p>
            <p class="flats-tabel--yardage"><?php the_field('yardage_a21') ?></p>
            <p class="flats-tabel--yardage"><?php the_field('yardage_a22') ?></p>
            <p class="flats-tabel--yardage"><?php the_field('yardage_a23') ?></p>
            <p class="flats-tabel--yardage"><?php the_field('yardage_a24') ?></p>
            <p class="flats-tabel--yardage"><?php the_field('yardage_a25') ?></p>
            <p class="flats-tabel--yardage"><?php the_field('yardage_b21') ?></p>
            <p class="flats-tabel--yardage"><?php the_field('yardage_b22') ?></p>
            <p class="flats-tabel--yardage"><?php the_field('yardage_b23') ?></p>
            <p class="flats-tabel--yardage"><?php the_field('yardage_b24') ?></p>
            <p class="flats-tabel--yardage"><?php the_field('yardage_b25') ?></p>
            <p class="flats-tabel--yardage"><?php the_field('yardage_b26') ?></p>
            <p class="flats-tabel--yardage"><?php the_field('yardage_a31') ?></p>
            <p class="flats-tabel--yardage"><?php the_field('yardage_a32') ?></p>
            <p class="flats-tabel--yardage"><?php the_field('yardage_a33') ?></p>
            <p class="flats-tabel--yardage"><?php the_field('yardage_a34') ?></p>
            <p class="flats-tabel--yardage"><?php the_field('yardage_a35') ?></p>
            <p class="flats-tabel--yardage"><?php the_field('yardage_b31') ?></p>
            <p class="flats-tabel--yardage"><?php the_field('yardage_b32') ?></p>
            <p class="flats-tabel--yardage"><?php the_field('yardage_b33') ?></p>
            <p class="flats-tabel--yardage"><?php the_field('yardage_b34') ?></p>
            <p class="flats-tabel--yardage"><?php the_field('yardage_b35') ?></p>
            <p class="flats-tabel--yardage"><?php the_field('yardage_b36') ?></p>
        </div>
        <div class="flats-tabel__col-five">        
            <p class="flats-tabel--title">Status</p>
            <p class="flats-tabel--status"><?php the_field('status_a01') ?></p>
            <p class="flats-tabel--status"><?php the_field('status_a02') ?></p>
            <p class="flats-tabel--status"><?php the_field('status_b01') ?></p>
            <p class="flats-tabel--status"><?php the_field('status_b02') ?></p>
            <p class="flats-tabel--status"><?php the_field('status_b03') ?></p>
            <p class="flats-tabel--status"><?php the_field('status_a11') ?></p>
            <p class="flats-tabel--status"><?php the_field('status_a12') ?></p>
            <p class="flats-tabel--status"><?php the_field('status_a13') ?></p>
            <p class="flats-tabel--status"><?php the_field('status_a14') ?></p>
            <p class="flats-tabel--status"><?php the_field('status_a15') ?></p>
            <p class="flats-tabel--status"><?php the_field('status_b11') ?></p>
            <p class="flats-tabel--status"><?php the_field('status_b12') ?></p>
            <p class="flats-tabel--status"><?php the_field('status_b13') ?></p>
            <p class="flats-tabel--status"><?php the_field('status_b14') ?></p>
            <p class="flats-tabel--status"><?php the_field('status_b15') ?></p>
            <p class="flats-tabel--status"><?php the_field('status_b16') ?></p>
            <p class="flats-tabel--status"><?php the_field('status_a21') ?></p>
            <p class="flats-tabel--status"><?php the_field('status_a22') ?></p>
            <p class="flats-tabel--status"><?php the_field('status_a23') ?></p>
            <p class="flats-tabel--status"><?php the_field('status_a24') ?></p>
            <p class="flats-tabel--status"><?php the_field('status_a25') ?></p>
            <p class="flats-tabel--status"><?php the_field('status_b21') ?></p>
            <p class="flats-tabel--status"><?php the_field('status_b22') ?></p>
            <p class="flats-tabel--status"><?php the_field('status_b23') ?></p>
            <p class="flats-tabel--status"><?php the_field('status_b24') ?></p>
            <p class="flats-tabel--status"><?php the_field('status_b25') ?></p>
            <p class="flats-tabel--status"><?php the_field('status_b26') ?></p>
            <p class="flats-tabel--status"><?php the_field('status_a31') ?></p>
            <p class="flats-tabel--status"><?php the_field('status_a32') ?></p>
            <p class="flats-tabel--status"><?php the_field('status_a33') ?></p>
            <p class="flats-tabel--status"><?php the_field('status_a34') ?></p>
            <p class="flats-tabel--status"><?php the_field('status_a35') ?></p>
            <p class="flats-tabel--status"><?php the_field('status_b31') ?></p>
            <p class="flats-tabel--status"><?php the_field('status_b32') ?></p>
            <p class="flats-tabel--status"><?php the_field('status_b33') ?></p>
            <p class="flats-tabel--status"><?php the_field('status_b34') ?></p>
            <p class="flats-tabel--status"><?php the_field('status_b35') ?></p>
            <p class="flats-tabel--status"><?php the_field('status_b36') ?></p>
        </div>
        <div class="flats-tabel__col-six">        
            <p class="flats-tabel--title">Karta Mieszkania</p>
            <a href="<?php the_field('card_a01') ?>" target="_blank">
                <p class="flats-tabel--card">Zobacz plan <i class="fa fa-search" aria-hidden="true"></i></p>
             </a>
            <a href="<?php the_field('card_a02') ?>" target="_blank">
                <p class="flats-tabel--card">Zobacz plan <i class="fa fa-search" aria-hidden="true"></i></p>
            </a>
            <a href="<?php the_field('card_b01') ?>" target="_blank">
                <p class="flats-tabel--card">Zobacz plan <i class="fa fa-search" aria-hidden="true"></i></p>
            </a>
            <a href="<?php the_field('card_b02') ?>" target="_blank">
                <p class="flats-tabel--card">Zobacz plan <i class="fa fa-search" aria-hidden="true"></i></p>
            </a>
            <a href="<?php the_field('card_b03') ?>" target="_blank">
                <p class="flats-tabel--card">Zobacz plan <i class="fa fa-search" aria-hidden="true"></i></p>
             </a>
            <a href="<?php the_field('card_a11') ?>" target="_blank">
                <p class="flats-tabel--card">Zobacz plan <i class="fa fa-search" aria-hidden="true"></i></p>
            </a>
            <a href="<?php the_field('card_a12') ?>" target="_blank">
                <p class="flats-tabel--card">Zobacz plan <i class="fa fa-search" aria-hidden="true"></i></p>
            </a>
            <a href="<?php the_field('card_a13') ?>" target="_blank">
                <p class="flats-tabel--card">Zobacz plan <i class="fa fa-search" aria-hidden="true"></i></p>
            </a>
            <a href="<?php the_field('card_a14') ?>" target="_blank">
                <p class="flats-tabel--card">Zobacz plan <i class="fa fa-search" aria-hidden="true"></i></p>
             </a>
            <a href="<?php the_field('card_a15') ?>" target="_blank">
                <p class="flats-tabel--card">Zobacz plan <i class="fa fa-search" aria-hidden="true"></i></p>
            </a>
            <a href="<?php the_field('card_b11') ?>" target="_blank">
                <p class="flats-tabel--card">Zobacz plan <i class="fa fa-search" aria-hidden="true"></i></p>
            </a>
            <a href="<?php the_field('card_b12') ?>" target="_blank">
                <p class="flats-tabel--card">Zobacz plan <i class="fa fa-search" aria-hidden="true"></i></p>
            </a>
            <a href="<?php the_field('card_b13') ?>" target="_blank">
                <p class="flats-tabel--card">Zobacz plan <i class="fa fa-search" aria-hidden="true"></i></p>
             </a>
            <a href="<?php the_field('card_b14') ?>" target="_blank">
                <p class="flats-tabel--card">Zobacz plan <i class="fa fa-search" aria-hidden="true"></i></p>
            </a>
            <a href="<?php the_field('card_b15') ?>" target="_blank">
                <p class="flats-tabel--card">Zobacz plan <i class="fa fa-search" aria-hidden="true"></i></p>
            </a>
            <a href="<?php the_field('card_b16') ?>" target="_blank">
                <p class="flats-tabel--card">Zobacz plan <i class="fa fa-search" aria-hidden="true"></i></p>
            </a>
            <a href="<?php the_field('card_a21') ?>" target="_blank">
                <p class="flats-tabel--card">Zobacz plan <i class="fa fa-search" aria-hidden="true"></i></p>
             </a>
            <a href="<?php the_field('card_a22') ?>" target="_blank">
                <p class="flats-tabel--card">Zobacz plan <i class="fa fa-search" aria-hidden="true"></i></p>
            </a>
            <a href="<?php the_field('card_a23') ?>" target="_blank">
                <p class="flats-tabel--card">Zobacz plan <i class="fa fa-search" aria-hidden="true"></i></p>
            </a>
            <a href="<?php the_field('card_a24') ?>" target="_blank">
                <p class="flats-tabel--card">Zobacz plan <i class="fa fa-search" aria-hidden="true"></i></p>
            </a>
            <a href="<?php the_field('card_a25') ?>" target="_blank">
                <p class="flats-tabel--card">Zobacz plan <i class="fa fa-search" aria-hidden="true"></i></p>
             </a>
            <a href="<?php the_field('card_b21') ?>" target="_blank">
                <p class="flats-tabel--card">Zobacz plan <i class="fa fa-search" aria-hidden="true"></i></p>
            </a>
            <a href="<?php the_field('card_b22') ?>" target="_blank">
                <p class="flats-tabel--card">Zobacz plan <i class="fa fa-search" aria-hidden="true"></i></p>
            </a>
            <a href="<?php the_field('card_b23') ?>" target="_blank">
                <p class="flats-tabel--card">Zobacz plan <i class="fa fa-search" aria-hidden="true"></i></p>
            </a>
            <a href="<?php the_field('card_b24') ?>" target="_blank">
                <p class="flats-tabel--card">Zobacz plan <i class="fa fa-search" aria-hidden="true"></i></p>
             </a>
            <a href="<?php the_field('card_b25') ?>" target="_blank">
                <p class="flats-tabel--card">Zobacz plan <i class="fa fa-search" aria-hidden="true"></i></p>
            </a>
            <a href="<?php the_field('card_b26') ?>" target="_blank">
                <p class="flats-tabel--card">Zobacz plan <i class="fa fa-search" aria-hidden="true"></i></p>
            </a>
            <a href="<?php the_field('card_a31') ?>" target="_blank">
                <p class="flats-tabel--card">Zobacz plan <i class="fa fa-search" aria-hidden="true"></i></p>
            </a>
            <a href="<?php the_field('card_a32') ?>" target="_blank">
                <p class="flats-tabel--card">Zobacz plan <i class="fa fa-search" aria-hidden="true"></i></p>
             </a>
            <a href="<?php the_field('card_a33') ?>" target="_blank">
                <p class="flats-tabel--card">Zobacz plan <i class="fa fa-search" aria-hidden="true"></i></p>
            </a>
            <a href="<?php the_field('card_a34') ?>" target="_blank">
                <p class="flats-tabel--card">Zobacz plan <i class="fa fa-search" aria-hidden="true"></i></p>
            </a>
            <a href="<?php the_field('card_a35') ?>" target="_blank">
                <p class="flats-tabel--card">Zobacz plan <i class="fa fa-search" aria-hidden="true"></i></p>
            </a>
            <a href="<?php the_field('card_b31') ?>" target="_blank">
                <p class="flats-tabel--card">Zobacz plan <i class="fa fa-search" aria-hidden="true"></i></p>
             </a>
            <a href="<?php the_field('card_b32') ?>" target="_blank">
                <p class="flats-tabel--card">Zobacz plan <i class="fa fa-search" aria-hidden="true"></i></p>
            </a>
            <a href="<?php the_field('card_b33') ?>" target="_blank">
                <p class="flats-tabel--card">Zobacz plan <i class="fa fa-search" aria-hidden="true"></i></p>
            </a>
            <a href="<?php the_field('card_b34') ?>" target="_blank">
                <p class="flats-tabel--card">Zobacz plan <i class="fa fa-search" aria-hidden="true"></i></p>
            </a>
            <a href="<?php the_field('card_b35') ?>" target="_blank">
                <p class="flats-tabel--card">Zobacz plan <i class="fa fa-search" aria-hidden="true"></i></p>
             </a>
            <a href="<?php the_field('card_b36') ?>" target="_blank">
                <p class="flats-tabel--card">Zobacz plan <i class="fa fa-search" aria-hidden="true"></i></p>
            </a>

        </div>
    </div>
</section>

