import "../css/style.css"
import $ from "jquery"
import Glide from "@glidejs/glide"
import MobileMenu from "./modules/MobileMenu"
import AOS from 'aos';
import 'aos/dist/aos.css';
import 'slick-carousel';
import "slick-carousel/slick/slick.css";
import 'lodash';


var mobileMenu = new MobileMenu()


if (module.hot) {
  module.hot.accept()
}
// aos

AOS.init();

// menu scroll

window.addEventListener("scroll", function(){
	var header = document.querySelector("header");
	header.classList.toggle("sticky", window.scrollY > 0)
  })

// gallery one

const $left = $(".left");
const $gl = $(".gallery");
const $gl2 = $(".gallery2");
const $photosCounterFirstSpan = $(".photos-counter span:nth-child(1)");

$gl2.on("init", (event, slick) => {
  $photosCounterFirstSpan.text(`${slick.currentSlide + 1}/`);
  $(".photos-counter span:nth-child(2)").text(slick.slideCount);
});

$gl.slick({
  rows: 0,
  slidesToShow: 2,
  arrows: false,
  draggable: false,
  useTransform: false,
  mobileFirst: true,
  responsive: [
    {
      breakpoint: 768,
      settings: {
        slidesToShow: 3
      }
    },
    {
      breakpoint: 1023,
      settings: {
        slidesToShow: 1,
        vertical: true
      }
    }
  ]
});

$gl2.slick({
  rows: 0,
  useTransform: false,
  prevArrow: ".arrow-left",
  nextArrow: ".arrow-right",
  fade: true,
  asNavFor: $gl
});

function handleCarouselsHeight() {
  if (window.matchMedia("(min-width: 1024px)").matches) {
    const gl2H = $(".gallery2").height();
    $left.css("height", gl2H);
  } else {
    $left.css("height", "auto");
  }
}

$(window).on("load", () => {
  handleCarouselsHeight();
  setTimeout(() => {
    $(".loading").fadeOut();
    $("body").addClass("over-visible");
  }, 300);
});

$(window).on(
  "resize",
  _.debounce(() => {
    handleCarouselsHeight();
  }, 200)
);

$(".gallery .item").on("click", function() {
  const index = $(this).attr("data-slick-index");
  $gl2.slick("slickGoTo", index);
});

$gl2.on("afterChange", (event, slick, currentSlide) => {
  $photosCounterFirstSpan.text(`${slick.currentSlide + 1}/`);
});

// gallery two
const $leftsite = $(".left-two");
const $gl_1 = $(".gallery_one");
const $gl_2 = $(".gallery_two");
const $photosCounterFirstSpanTwo = $(".photos-counter-two span:nth-child(1)");

$gl_2.on("init", (event, slick) => {
  $photosCounterFirstSpanTwo.text(`${slick.currentSlide + 1}/`);
  $(".photos-counter-two span:nth-child(2)").text(slick.slideCount);
});

$gl_1.slick({
  rows: 0,
  slidesToShow: 2,
  arrows: false,
  draggable: false,
  useTransform: false,
  mobileFirst: true,
  responsive: [
    {
      breakpoint: 768,
      settings: {
        slidesToShow: 3
      }
    },
    {
      breakpoint: 1023,
      settings: {
        slidesToShow: 1,
        vertical: true
      }
    }
  ]
});

$gl_2.slick({
  rows: 0,
  useTransform: false,
  prevArrow: ".arrow-left-two",
  nextArrow: ".arrow-right-two",
  fade: true,
  asNavFor: $gl_1
});

function handleCarouselsHeightTwo() {
  if (window.matchMedia("(min-width: 1024px)").matches) {
    const gl2H = $(".gallery_two").height();
    $leftsite.css("height", gl2H);
  } else {
    $leftsite.css("height", "auto");
  }
}

$(window).on("load", () => {
  handleCarouselsHeightTwo();
  setTimeout(() => {
    $(".loading").fadeOut();
    $("body").addClass("over-visible");
  }, 300);
});

$(window).on(
  "resize",
  _.debounce(() => {
    handleCarouselsHeightTwo();
  }, 200)
);

$(".gallery_one .item").on("click", function() {
  const index = $(this).attr("data-slick-index");
  $gl_2.slick("slickGoTo", index);
});

$gl_2.on("afterChange", (event, slick, currentSlide) => {
  $photosCounterFirstSpanTwo.text(`${slick.currentSlide + 1}/`);
});


  // contact form
  $(document).ready(function () {
    $('.gallery-one').show()
    $('.gallery-two').hide();
    $('.btn-one').addClass('active');

    $('.btn-one').on('click', function () {
      $('.gallery-one').show();
      $('.gallery-two').hide();
      $('.btn-one').addClass('active');
      $('.btn-two').removeClass('active');
    });
    $('.btn-two').on('click', function () {
      $('.gallery-one').hide();
      $('.gallery-two').show();
      $('.btn-two').addClass('active');
      $('.btn-one').removeClass('active');
    });
  });

  // carousel

  new Glide('#glide1', {
    type: 'carousel',
    autoplay: 2000,
    focusAt: 'center',
    perView: 3,
    breakpoints: {
      800: {
        perView: 1
      }
    }
  }).mount();
